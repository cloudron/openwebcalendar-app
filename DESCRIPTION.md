### Overview

The Open Web Calendar uses ICS/ICal calendars online and displays them in one calendar. You can use it with Nextcloud, Outlook, Google Calendar, Meetup and other calendar systems using the ICS standard.
