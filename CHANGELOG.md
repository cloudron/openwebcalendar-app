[0.1.0]
* Initial version for 1.20.0
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.20)

[0.2.0]
* Update openwebcalendar to 1.21
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.21)
* Update Chinese translation by dingc
* Update French translation by Thomas Moerschell
* Fix Content-Type header for .js files, see Issue 241
* Add logo Issue 205

[0.3.0]
* Update openwebcalendar to 1.22
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.22)
* Update dependencies

[0.4.0]
* Update openwebcalendar to 1.23
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.23)

[1.0.0]
* Initial stable package version for openwebcalendar 1.23

[1.0.1]
* Update openwebcalendar to 1.24
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.24)
* log changes

[1.0.2]
* Update openwebcalendar to 1.25
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.25)
* Update dependencies

[1.0.3]
* Update openwebcalendar to 1.26
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.26)
* Test language choice

[1.0.4]
* Update openwebcalendar to 1.27
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.27)

[1.0.5]
* Update openwebcalendar to 1.28
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.28)

[1.0.6]
* Update openwebcalendar to 1.29
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.29)

[1.0.7]
* Update openwebcalendar to 1.30
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.30)

[1.0.8]
* Update openwebcalendar to 1.31
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.31)

[1.0.9]
* Update openwebcalendar to 1.32
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.32)
* Add feature tests for link targets in various locations by @niccokunzmann in https://github.com/niccokunzmann/open-web-calendar/pull/327

[1.0.10]
* Update openwebcalendar to 1.33
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.33)
* Update dependencies
* Add alternate link to “text/calendar” content to ease subscribing for other calendars, see Issue 308
* Use calendar feeds from alternate links to “text/calendar” content, see Issue 309
* Use HTML description from various sources, see Issue 300
* Allow JavaScript customization of the calendar, see Issue 71
* Improve Portuguese by qeepoo

[1.0.11]
* Update openwebcalendar to 1.34
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.34)
* Ensures every HTML document has a lang attribute (html) Issue 347
* Allow hosters to close the Host Header Injection vulnerability, see PR 366
* Improve documentation and Docker build
* Add Esperanto by phlostically
* Improve Slovak, Russian, Portuguese, Indonesian by phlostically
* Update requirements

[1.1.0]
* Update openwebcalendar to 1.35
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.35)

[1.2.0]
* Update openwebcalendar to 1.36
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.36)

[1.2.1]
* Update openwebcalendar to 1.37
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.37)

[1.2.2]
* Update openwebcalendar to 1.38
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.38)

[1.2.3]
* Update openwebcalendar to 1.39
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.39)

[1.2.4]
* Update openwebcalendar to 1.40
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.40)
* Support webcal:// links
* Provide docker releases with proper tag release information
* Add choice: Choose the language of the viewer
* Update DHTMLX Scheduler to v7.1.1
* Update examples to work with DHTMLX Scheduler v7.1.1
* Add script to automatically update DHTMLX Scheduler
* Update dependencies
* Add Chinese (Traditional Han script) and Korean
* Update Hebrew by Ori, Chinese (Traditional Han script) by 張可揚, Spanish by gallegonovato, German by Nicco Kunzmann, Slovak by Milan Šalka and Nicco Kunzmann, Croatian by Milo Ivir and Welsh by Siaron James
* Fix links to new location in the open_web_calendar subdirectory

[1.2.5]
* Update openwebcalendar to 1.41
* [Full changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.41)
* Update dependencies
* Use renovate to auto-update dependencies
* Update Spanish by gallegonovato
* Improve funding documentation
* fix: use flask-allowed-hosts because flask-allowedhosts was deleted

[1.3.0]
* Update open-web-calendar to 1.42
* [Full Changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.42)

[1.4.0]
* Update open-web-calendar to 1.43
* [Full Changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.43)

[1.5.0]
* Update open-web-calendar to 1.44
* [Full Changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.44)

[1.6.0]
* Update open-web-calendar to 1.45
* [Full Changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.45)

[1.7.0]
* Update open-web-calendar to 1.46
* [Full Changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.46)

[1.8.0]
* Update open-web-calendar to 1.47
* [Full Changelog](https://github.com/niccokunzmann/open-web-calendar/releases/tag/v1.47)
* Add option to download events in ICS format, see Issue 206
* Remove pytz as dependency, replace it by zoneinfo
* Update dependencies
* Update Ukrainian translation by Максим Горпиніч, Hungarian by Vág Csaba, Finnish by Ricky Tigg, German by Nicco Kunzmann
* Describe how to use the Squid proxy to protect from SSRF attacks.

