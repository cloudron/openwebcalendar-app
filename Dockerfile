FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

ENV WORKERS=4
ENV PYTHONUNBUFFERED=true
ENV APP_DEBUG=true

# renovate: datasource=github-releases depName=niccokunzmann/open-web-calendar versioning=regex:^(?<major>\d+)\.(?<minor>\d+) extractVersion=^v(?<version>.+)$
ARG OWC_VERSION=1.47

RUN curl -L https://github.com/niccokunzmann/open-web-calendar/archive/refs/tags/v${OWC_VERSION}.tar.gz | tar zxf - --strip-components 1 -C /app/code

RUN cp /app/code/docker/constraints.txt /app/code/constraints.txt
RUN export PIP_CONSTRAINT=constraints.txt
RUN cp /app/code/requirements/base.txt /app/code/requirements.txt

RUN virtualenv -p python3 ENV
RUN source ENV/bin/activate
RUN pip install --upgrade --no-cache-dir -r requirements.txt

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
