#!/bin/bash

cd /app/code

source ENV/bin/activate
gunicorn -w "$WORKERS" -b "0.0.0.0:3000" --access-logfile "-" --log-level "debug" app:app